using UnityEngine;

public class PlayingFields : MonoBehaviour
{
    [SerializeField] private Field[] fields;

    // Start is called before the first frame update
    void Start()
    {
        GetAllFields();
    }

    [ExecuteInEditMode]
    public Field[] GetAllFields()
    {
        //fields = GetComponentsInChildren<Field>();
        return fields;
    }

    public void ResetObject()
    {
        foreach (Field field in fields)
        {
            field.ChangeColor(Color.white);
        }
    }

    public Field GetClickedField(GameObject clickedFieldGameObject)
    {
        foreach (Field field in fields)
        {
            if (field.gameObject == clickedFieldGameObject)
            {
                return field;
            }
        }

        return null;
    }

    // Update is called once per frame
}