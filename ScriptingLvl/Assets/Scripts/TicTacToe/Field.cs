using UnityEngine;

public class Field : MonoBehaviour
{
    Color currentColor = Color.white;
    [SerializeField] private Renderer fieldRenderer;

    private void Start()
    {
        currentColor = GetComponent<Renderer>().material.color;
    }

    public void ChangeColor(Color color)
    {
        fieldRenderer.material.color = color;
        currentColor = color;
    }

    public Color GetCurrentColor()
    {
        return currentColor;
    }
}