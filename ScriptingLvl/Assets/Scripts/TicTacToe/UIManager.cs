using TMPro;
using UnityEngine;

namespace TicTacToe
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI headerText;
        [SerializeField] private GameManager gameManager;
        [SerializeField] public GameObject gameOverUI;

        private void Awake()
        {
            gameManager ??= FindObjectOfType<GameManager>();
        }

        private void Start()
        {
            UpdateHeaderText("Player One's Turn");
        }

        private void OnEnable()
        {
            gameManager.GameOver += OnGameOver;
            gameManager.PlayerChanged += OnPlayerChanged;
            gameManager.UpdateWinner += OnUpdateWinner;
        }

        private void OnDestroy()
        {
            gameManager.GameOver -= OnGameOver;
            gameManager.PlayerChanged -= UpdateHeaderText;
            gameManager.UpdateWinner -= UpdateHeaderText;
        }

        private void OnUpdateWinner(string text)
        {
            UpdateHeaderText(text);
        }

        private void OnPlayerChanged(string text)
        {
            UpdateHeaderText(text);
        }

        private void UpdateHeaderText(string text)
        {
            headerText.text = text;
        }


        private void OnGameOver(bool isPlayerOne, bool isDraw)
        {
            gameOverUI.SetActive(true);
            if (isDraw)
            {
                headerText.text = "Draw";
                return;
            }

            headerText.text = isPlayerOne ? "Player One Win" : "Player Two Win";
        }

        public void ResetObject()
        {
            UpdateHeaderText("Player One's Turn");
            gameOverUI.SetActive(false);
        }
    }
}