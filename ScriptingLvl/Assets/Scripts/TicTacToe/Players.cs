using System;
using UnityEngine;

public class Players : MonoBehaviour
{
    [SerializeField] public Color playerOneColor = Color.blue;
    [SerializeField] public Color playerTwoColor = Color.red;


    //public event Action<bool> PlayerChanged;
    public event Action<RaycastHit2D> ExecuteTurn;

    public bool isPlayerOne;


    void Start()
    {
        isPlayerOne = true;
    }

    public bool DoActivity(Camera mainCamera)
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D hit = Physics2D.Raycast(mainCamera.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

            ExecuteTurn?.Invoke(hit);
            return true;
        }

        return false;
    }


    public Color PlayerColor(bool isPlayerOne)
    {
        if (isPlayerOne)
        {
            return playerOneColor;
        }
        else
        {
            return playerTwoColor;
        }
    }
}