using System;
using UnityEngine;

namespace TicTacToe
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private Players players;
        [SerializeField] private PlayingFields playingFields;
        [SerializeField] public Camera mainCamera;
        public event Action<string> PlayerChanged;
        public event Action<bool, bool> GameOver;
        public event Action<string> UpdateWinner;
        private bool isGameOver;

        //public delegate bool CheckWinCondition(Color playerColor, bool isPlayerOne);

        //public CheckWinCondition checkWinCondition;

        private void Awake()
        {
            mainCamera ??= Camera.main;
            players ??= FindObjectOfType<Players>();
            playingFields ??= FindObjectOfType<PlayingFields>();
        }

        private void OnEnable()
        {
            players.ExecuteTurn += OnExecuteTurn;

            //players.PlayerChanged += OnPlayerChanged;
            // players.checkWinCondition += OnCheckWinCondition;
        }

        private void OnDisable()
        {
            //players.PlayerChanged -= OnPlayerChanged;
            // players.checkWinCondition -= OnCheckWinCondition;
        }

        private void OnPlayerChanged(bool isPlayerOne)
        {
            string text = "Player Two's Turn";
            if (isPlayerOne)
            {
                text = "Player One's Turn";
            }

            PlayerChanged?.Invoke(text);
        }

        private void Update()
        {
            if (isGameOver) return;
            players.DoActivity(mainCamera);
        }


        public void OnExecuteTurn(RaycastHit2D hit)
        {
            if (hit.collider is null)
            {
            }
            else
            {
                Field field = playingFields.GetClickedField(hit.collider.gameObject);
                Color fieldColor = field.GetCurrentColor();
                if (fieldColor != players.playerOneColor && fieldColor != players.playerTwoColor)
                {
                    field.ChangeColor(players.PlayerColor(players.isPlayerOne));

                    if (players.isPlayerOne)
                    {
                        isGameOver = OnCheckWinCondition(players.playerOneColor, true);
                    }
                    else
                    {
                        isGameOver = OnCheckWinCondition(players.playerTwoColor, false);
                    }

                    if (isGameOver) return;
                    players.isPlayerOne = !players.isPlayerOne;
                    OnPlayerChanged(players.isPlayerOne);
                }
            }
        }


        private bool OnCheckWinCondition(Color playerColor, bool isPlayerOne)
        {
            string text = "Player Two Win";
            Field[] fields = playingFields.GetAllFields();

            if (isPlayerOne)
            {
                text = "Player One Win";
            }

            UpdateWinner?.Invoke(text);

            bool temp = false;

            //baris 1 
            if (fields[0].GetCurrentColor() == playerColor && fields[1].GetCurrentColor() == playerColor &&
                fields[2].GetCurrentColor() == playerColor)
            {
                temp = true;
            }
            else if (fields[3].GetCurrentColor() == playerColor && fields[4].GetCurrentColor() == playerColor &&
                     fields[5].GetCurrentColor() == playerColor
                    )
            {
                //baris 2
                temp = true;
            }
            else if (fields[8].GetCurrentColor() == playerColor && fields[6].GetCurrentColor() == playerColor &&
                     fields[7].GetCurrentColor() == playerColor)
            {
                // baris 3
                temp = true;
            }
            else if (fields[0].GetCurrentColor() == playerColor && fields[3].GetCurrentColor() == playerColor &&
                     fields[6].GetCurrentColor() == playerColor)
            {
                // kolom 1
                temp = true;
            }
            else if (fields[1].GetCurrentColor() == playerColor && fields[4].GetCurrentColor() == playerColor &&
                     fields[7].GetCurrentColor() == playerColor)
            {
                //kolom 2
                temp = true;
            }
            else if (fields[2].GetCurrentColor() == playerColor && fields[5].GetCurrentColor() == playerColor &&
                     fields[8].GetCurrentColor() == playerColor)
            {
                //kolom 3
                temp = true;
            }
            else if (fields[0].GetCurrentColor() == playerColor && fields[4].GetCurrentColor() == playerColor &&
                     fields[8].GetCurrentColor() == playerColor)
            {
                //diagonal 1
                temp = true;
            }
            else if (fields[2].GetCurrentColor() == playerColor && fields[4].GetCurrentColor() == playerColor &&
                     fields[6].GetCurrentColor() == playerColor)
            {
                //diagonal 2
                temp = true;
            }

            if (temp == true)
            {
                GameOver?.Invoke(isPlayerOne, false);
                return true;
            }

            bool isAllFieldColored = true;
            foreach (Field field in fields)
            {
                if (field.GetCurrentColor() == Color.white)
                {
                    isAllFieldColored = false;
                }
            }

            if (isAllFieldColored)
            {
                GameOver?.Invoke(isPlayerOne, true);
                return true;
            }

            return false;
        }

        public void ResetObject()
        {
            isGameOver = false;
            players.isPlayerOne = true;
            playingFields.ResetObject();
        }
    }
}