using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour
{
    [SerializeField] GameObject wallPrefab;
    [SerializeField]  GameObject coinPrefab;
    [SerializeField] GameObject boosterPrefab;
    [SerializeField] GameObject floorPrefab;
    [SerializeField] Transform[] pos;

    [SerializeField] float rangeOffset = 30f;

    float minSpawnTime = 0.5f;
    float maxSpawnTime = 2f;

    float spawnTime;
    Player player;
    private GameManager gameManager;


    // Untuk object pooling
    public static ObstacleSpawner SharedInstance;
    [SerializeField] int numberToPool = 20;
    [SerializeField] public List<GameObject> coinPool = new List<GameObject>();
    [SerializeField] public List<GameObject> wallPool = new List<GameObject>();
    [SerializeField] public List<GameObject> boosterPool = new List<GameObject>();
    [SerializeField] public List<GameObject> floorPool = new List<GameObject>();
    [SerializeField] int numberToPoolForFloor = 3;

    //
    private void Awake()
    {
        SharedInstance = this;
    }

    private void Start()
    {
        
        gameManager = FindObjectOfType<GameManager>();
        player = FindObjectOfType<Player>();
        SetSpawnTime();

        // wall pake object pooling
        PopulatePool(coinPool,coinPrefab);
        PopulatePool(boosterPool,boosterPrefab);
        PopulatePool(wallPool,wallPrefab);
        PopulatePool(floorPool,floorPrefab);
    }
  


    // Update is called once per frame
    void FixedUpdate()
    {
        if (gameManager.isGameOver) return;
        Spawn();
    }

    private void PopulatePool(List<GameObject> objectPoolList, GameObject objectPrefab) {
        
        for (int i = 0; i < numberToPool; i++)
        {
            GameObject gonnaBePoolObject = Instantiate(objectPrefab);
            gonnaBePoolObject.SetActive(false);
            objectPoolList.Add(gonnaBePoolObject);
        }
    }
    
    

    public GameObject GetPooledObject(List<GameObject> objectPoolList) {
        for (int i = 0; i < numberToPool; i++)
        {
            if (!objectPoolList[i].activeInHierarchy)
            {
                return objectPoolList[i];
            }
        }
        return null;
    }

    public void DeactiveAllPooledObject(List<GameObject> objectPoolList) {
        for (int i = 0; i < numberToPool; i++)
        {
            if (objectPoolList[i].activeInHierarchy)
            {
                 objectPoolList[i].gameObject.SetActive(false);
            }
        }
    }

    

    private void Spawn()
    {
        Vector3 playerPosition = new Vector3(0f, transform.position.y, player.transform.position.z);
        transform.position = playerPosition;
        spawnTime -= Time.deltaTime;
        if (spawnTime <= 0)
        {
            Vector3 positionOffSet = new Vector3(0f, 0f, rangeOffset);
            int obstacleIndex = Random.Range(0, pos.Length);

            int coinIndex = Random.Range(0, pos.Length);
            
            while (coinIndex == obstacleIndex) {
                coinIndex = Random.Range(0, pos.Length);
            }

            if (Random.Range(0, 5) == 0)
            {

                int boosterIndex = Random.Range(0, pos.Length);
                while (coinIndex == boosterIndex || boosterIndex == obstacleIndex)
                {
                    boosterIndex = Random.Range(0, pos.Length);
                }

                SpawnObject(positionOffSet,boosterIndex, boosterPool);
                //Instantiate(boosterPrefab, pos[boosterIndex].position + positionOffSet, Quaternion.Euler(0, 0, 25f));
            }


            SpawnObject(positionOffSet, coinIndex, coinPool);



            //Instantiate(coinPrefab, pos[coinIndex].position + positionOffSet, Quaternion.identity);
            SpawnObject(positionOffSet, obstacleIndex,wallPool);

            //Instantiate(wallPrefab, pos[obstacleIndex].position + positionOffSet, Quaternion.identity);

            SetSpawnTime();
        }
    }

    private void SpawnObject(Vector3 positionOffSet, int index, List<GameObject> objectPoolList)
    {
        GameObject instantiatedBooster = GetPooledObject(objectPoolList);
        instantiatedBooster.SetActive(true);
        instantiatedBooster.transform.position = pos[index].position + positionOffSet;
    }

    public void SetSpawnTime()
    {
        spawnTime = Random.Range(minSpawnTime, maxSpawnTime);

    }
}
