using System;
using UnityEngine;


public class GameManager : MonoBehaviour
{
    Player player;
    ObstacleSpawner obstacleSpawner;
    public bool isGameOver;
    public float score;
    public event Action GameOver;
    public event Action<string> ScoreChanged;
    


    private void Awake()
    {
        obstacleSpawner = FindObjectOfType<ObstacleSpawner>();
        player = FindObjectOfType<Player>();
        player.HealthChanged += OnHealthChanged;
    }

    private void Start()
    {
        player.HealthChanged += OnHealthChanged;
        player.CollectCoin += OnCollectCoin;
    }

    private void OnHealthChanged(int health)
    {
        if (health <= 0)
        {
            isGameOver = true; 
            player.gameObject.SetActive(false);
            GameOver?.Invoke();
        }
    }

    void Update()
    {
        // if (Input.GetKeyDown(KeyCode.R))
        // {
        //     ResetGame();
        //     //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        // }
    }

    private void FixedUpdate()
    {
        if (isGameOver == true) return;
        score += Time.deltaTime;
    }

    public void ResetGame()
    {
        obstacleSpawner.DeactiveAllPooledObject(obstacleSpawner.coinPool);
        obstacleSpawner.DeactiveAllPooledObject(obstacleSpawner.boosterPool);
        obstacleSpawner.DeactiveAllPooledObject(obstacleSpawner.wallPool);


        player.gameObject.SetActive(true);
        player.ResetObject();
        isGameOver = false;
    }

    public void OnCollectCoin(float coinScore)
    {
        score += coinScore;
    }

    // public void ResetObject()
    // {
    //     //player.HitObstacle += OnHealthChanged;
    //     player.CollectCoin += OnCollectCoin;
    //     score = 0;
    //
    //     isGameOver = false;
    // }

    public string GetScoreText()
    {
        return "Score : " + Mathf.FloorToInt(score).ToString();
    }
}