using UnityEngine;

public class Coin : DestroyableObject
{
    [SerializeField] float coinScore = 5f;

    protected override void Update()
    {
        if ((Camera.main.WorldToViewportPoint(transform.position).z <= destroyRangeOffset))
        {
            gameObject.SetActive(false);
        }
    }

    protected override void OnBecameInvisible()
    {
        gameObject.SetActive(false);
    }

    public float GetCoinScore()
    {
        return coinScore;
    }
}