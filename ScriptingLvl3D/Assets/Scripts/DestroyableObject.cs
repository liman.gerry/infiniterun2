using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyableObject : MonoBehaviour
{
    [SerializeField] protected float destroyRangeOffset = -4f;

    protected virtual void Update()
    {
        if ((Camera.main.WorldToViewportPoint(transform.position).z <= destroyRangeOffset))
        {
            gameObject.SetActive(false);
        }

    }

    protected virtual void OnBecameInvisible()
    {
        gameObject.SetActive(false);



    }
}
