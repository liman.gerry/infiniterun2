using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UIElements;

public class UIKeeper : MonoBehaviour, IResetAble
{
    [SerializeField] private TextMeshProUGUI scoreTextUI;
    [SerializeField] private TextMeshProUGUI healthTextUI;
    [SerializeField] private GameObject ResetButton;

    private GameManager gameManager;

    private Player player;


    
    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        
        player = FindObjectOfType<Player>();
        gameManager.GameOver += OnGameOver;
        UpdateScoreText(gameManager.GetScoreText());
        player.HealthChanged += OnHealthChanged;
        //gameManager.ScoreChanged += UpdateScoreText;
    }

    private void OnHealthChanged(int health)
    {
        healthTextUI.text = "Health : " + health;
    }

    private void OnGameOver()
    {
        ResetButton.SetActive(true);
    }

    

    private void FixedUpdate()
    {
        UpdateScoreText(gameManager.GetScoreText());
    }

    private void UpdateScoreText(string scoreText)
    {
        this.scoreTextUI.text = scoreText;
    }


    public void ResetObject()
    {
        //
        healthTextUI.text = "Health : 3";
        this.scoreTextUI.text = "Score : 0";
        ResetButton.SetActive(false);
    }

   
}