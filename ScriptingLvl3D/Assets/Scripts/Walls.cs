using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Walls : DestroyableObject
{
    protected override void Update()
    {
        if ((Camera.main.WorldToViewportPoint(transform.position).z <= destroyRangeOffset))
        {
            gameObject.SetActive(false);
            //Destroy(gameObject);
        }

        //base.Update();
    }
    protected override void OnBecameInvisible()
    {
        gameObject.SetActive(false);


    }
}
