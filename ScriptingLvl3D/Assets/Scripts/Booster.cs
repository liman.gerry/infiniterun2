using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Booster : DestroyableObject
{
    [SerializeField] float speedGain = 5f;
    [SerializeField] float duration = 3f;

    public float GetSpeedGain() {
        return speedGain;
    }

    public float GetBoostDuration() {
        return duration;
    }

    protected override void Update()
    {
        if ((Camera.main.WorldToViewportPoint(transform.position).z <= destroyRangeOffset))
        {
            gameObject.SetActive(false);
            //Destroy(gameObject);
        }

        base.Update();
    }
    protected override void OnBecameInvisible()
    {
        gameObject.SetActive(false);


    }


}
