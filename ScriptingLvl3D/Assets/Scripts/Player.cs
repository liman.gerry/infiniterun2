using System;
using UnityEngine;

public class Player : MonoBehaviour, IResetAble
{
    [SerializeField] float offsetX;
    [SerializeField] float timeToStrafe = 0.1f;
    float timeCounterForStrafe;
    [SerializeField] float strafeSpeed = 12f;

    Rigidbody playerRigidBody;
    [SerializeField] float forwardSpeedNormal = 1.5f;
    [SerializeField] float forwardSpeedBoost = 3f;


    private float forwardSpeed;
    public event Action<int> HealthChanged;
    public event Action<float> CollectCoin;
    bool isStrafing = false;
    Vector3 startPos;
    Vector3 lastPos;

    float boostDuration = 0f;
    float boostSpeed = 0f;

    private const int defaultHealth = 3;
    private int health;

    // Start is called before the first frame update
    void Start()
    {
        health = defaultHealth;
        forwardSpeed = forwardSpeedNormal;
        playerRigidBody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        MovePlayer();


        //transform.Translate((Vector3.forward * forwardSpeed) * Time.deltaTime);
    }

    private void MovePlayer()
    {
        CheckBoost();
        CalculateBoost();

        float strafingDirection = offsetX;


        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (playerRigidBody.position.x > -offsetX && isStrafing == false)
            {
                isStrafing = true;
                // playerRigidBody.position = playerRigidBody.position - offsetVector;
                strafingDirection = -offsetX;
                startPos = playerRigidBody.position;
                lastPos = new Vector3(startPos.x + strafingDirection, playerRigidBody.position.y,
                    playerRigidBody.position.z);
            }
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (playerRigidBody.position.x < offsetX && isStrafing == false)
            {
                isStrafing = true;
                strafingDirection = offsetX;
                startPos = playerRigidBody.position;
                lastPos = new Vector3(startPos.x + strafingDirection, playerRigidBody.position.y,
                    playerRigidBody.position.z);
                // playerRigidBody.position = playerRigidBody.position + offsetVector;
            }
        }

        Vector3 moveForward = new Vector3(0f, 0f, forwardSpeed + boostSpeed);

        //Vector3 offsetVector = new Vector3(strafingDirection, 0f, 0f);
        if (isStrafing)
        {
            lastPos.z = lastPos.z + (forwardSpeed * Time.deltaTime);
            if (timeCounterForStrafe < timeToStrafe)
            {
                playerRigidBody.position = Vector3.Lerp(startPos, lastPos, timeCounterForStrafe / timeToStrafe);

                timeCounterForStrafe += Time.deltaTime;
            }
            else
            {
                playerRigidBody.position = lastPos;
                isStrafing = false;
                timeCounterForStrafe = 0f;
            }
        }
        else
        {
            playerRigidBody.position = playerRigidBody.position + (moveForward * Time.deltaTime);
        }
    }

    private void CalculateBoost()
    {
        if (boostDuration > 0)
        {
            boostDuration -= Time.deltaTime;
        }
        else
        {
            boostSpeed = 0f;
        }
    }

    private void CheckBoost()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            forwardSpeed = forwardSpeedBoost;
        }
        else
        {
            forwardSpeed = forwardSpeedNormal;
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.collider.tag == "Walls")
        {
            DoDamage(1);


            //this.enabled = false;
            other.gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Coins")
        {
            CollectCoin.Invoke(other.transform.GetComponent<Coin>().GetCoinScore());
            other.gameObject.SetActive(false);
        }
        else if (other.transform.tag == "Booster")
        {
            boostSpeed = other.GetComponent<Booster>().GetSpeedGain();
            boostDuration = other.GetComponent<Booster>().GetBoostDuration();
            other.gameObject.SetActive(false);
        }
    }

    private void DoDamage(int damage)
    {
        health -= damage;
        HealthChanged?.Invoke(health);
    }

    public void ResetObject()
    {
        boostDuration = 0f;
        health = defaultHealth;
        playerRigidBody.position = new Vector3(0, playerRigidBody.position.y, playerRigidBody.position.z);
    }
}