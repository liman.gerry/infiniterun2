using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorSpawner : MonoBehaviour
{
    [SerializeField] GameObject floorPrefab;
    [SerializeField] Transform spawnLocation;
    [SerializeField] ObstacleSpawner obstacleSpawner;
    private void Start()
    {
        obstacleSpawner = FindObjectOfType<ObstacleSpawner>();
    }
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
           GameObject floorObject = obstacleSpawner.GetPooledObject(obstacleSpawner.floorPool);
            floorObject.SetActive(true);
            floorObject.transform.position = spawnLocation.position;
            //Instantiate(floorPrefab, spawnLocation.position, Quaternion.identity);
        }
    }
}
