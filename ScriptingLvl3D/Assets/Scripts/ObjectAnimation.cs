using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectAnimation : MonoBehaviour
{
    [SerializeField] float rotatingSpeed = 1f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 rotatingVector = new Vector3(0f,rotatingSpeed,0f);
        transform.Rotate(rotatingVector,Space.World);
    }
}
